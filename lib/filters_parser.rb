# frozen_string_literal: true

module FiltersParser
  KEY_MAPPINGS = {
    eq: '=',
    ne: '!=',
    gt: '>',
    lt: '<'
  }.freeze

  FILTERED_FIELDS = {
    distance: 'distance_m',
    time: 'time_sec',
    lat: 'lat',
    lng: 'lng',
    location: 'location',
    date: 'date',
    name: 'name',
    email: 'email',
    created_at: 'created_at'
  }.freeze

  FIELDS = %w[distance_m time_sec lat lng location date name email created_at].freeze
  OPERATORS = %w[= != > <].freeze
  LOGICAL_OPERATORS = %w[AND OR and or].freeze

  def self.parse(filters)
    return unless filters

    filters_parsed = filters
    KEY_MAPPINGS.each do |key, mapping|
      filters_parsed = filters_parsed.gsub(key.to_s, " #{mapping} ")
    end

    FILTERED_FIELDS.each do |key, field|
      filters_parsed = filters_parsed.gsub(key.to_s, " #{field} ")
    end

    filters_parsed = filters_parsed.gsub(')', " ) ")

    query, values = extract_values_from_query(filters_parsed)

    ActiveRecord::Base.sanitize_sql(([query] << values).flatten)
  end

  def self.extract_values_from_query(query)
    tokens = query.split
    values = []
    parsed_query = []
    tokens.each do |token|
      if FIELDS.include?(token) || OPERATORS.include?(token) || LOGICAL_OPERATORS.include?(token) || parenthesis?(token)
        parsed_query << token
      else
        values << token
        parsed_query << ' ? '
      end
    end

    [parsed_query.join(' '), values]
  end

  def self.parenthesis?(token)
    sanitized_token = token.gsub(/\s+/, '')

    !sanitized_token.match(/\(+/).nil? || !sanitized_token.match(/\)+/).nil?
  end
end
