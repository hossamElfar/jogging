# frozen_string_literal: true

module Weatherapi
  BASE_API = 'https://api.weatherapi.com/v1/'
  HTTP_TIMEOUT = 2

  def self.find_weather_history_by(conditions)
    q = if conditions[:lat].present? && conditions[:lng].present?
      "#{conditions[:lat]},#{conditions[:lng]}"
    elsif conditions[:location].present?
      conditions[:location]
    end
    date = conditions[:date]
    return [nil, -1] if q.blank? || date.blank?

    params = {
      q: q,
      dt: date,
      key: WEATHERAPI_API_KEY
    }

    begin
      response = RestClient.get "#{BASE_API}history.json", { params: params }
      parsed_response = JSON.parse response
      weather = parsed_response['forecast']['forecastday'].first['day']['avgtemp_c']

      [weather, 200]
    rescue RestClient::ExceptionWithResponse => e
      [nil, e.response.code]
    end
  end
end
