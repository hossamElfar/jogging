class CreateRuns < ActiveRecord::Migration[6.1]
  def change
    create_table :runs do |t|
      t.decimal :lat, precision: 10, scale: 6
      t.decimal :lng, precision: 10, scale: 6
      t.float :weather
      t.date :date
      t.float :time_sec
      t.float :distance_m
      t.belongs_to :user

      t.timestamps
    end
  end
end
