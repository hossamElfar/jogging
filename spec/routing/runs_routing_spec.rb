require "rails_helper"

RSpec.describe RunsController, type: :routing do
  describe "routing" do
    it "routes to #index" do
      expect(get: "/users/1/runs").to route_to("runs#index", user_id: "1")
    end

    it "routes to #show" do
      expect(get: "/users/1/runs/1").to route_to("runs#show", id: "1", user_id: "1")
    end

    it "routes to #create" do
      expect(post: "/users/1/runs").to route_to("runs#create", user_id: "1")
    end

    it "routes to #update via PUT" do
      expect(put: "/users/1/runs/1").to route_to("runs#update", id: "1", user_id: "1")
    end

    it "routes to #update via PATCH" do
      expect(patch: "/users/1/runs/1").to route_to("runs#update", id: "1", user_id: "1")
    end

    it "routes to #destroy" do
      expect(delete: "/users/1/runs/1").to route_to("runs#destroy", id: "1", user_id: "1")
    end
  end
end
