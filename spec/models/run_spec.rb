require 'rails_helper'

RSpec.describe Run, type: :model do
  context 'run validations' do
    let(:run) { build :run }

    context 'when required fileds is missing' do
      it 'is valid with valid attributes' do
        expect(run).to be_valid
      end

      it 'is not valid without a lat' do
        run.location = nil
        run.lat = nil
        expect(run).to_not be_valid
      end

      it 'is not valid without a lng' do
        run.location = nil
        run.lng = nil
        expect(run).to_not be_valid
      end

      it 'is not valid without a time_sec' do
        run.time_sec = nil
        expect(run).to_not be_valid
      end

      it 'is not valid without a distance_m' do
        run.distance_m = nil
        expect(run).to_not be_valid
      end

      it 'is not valid without a date' do
        run.date = nil
        expect(run).to_not be_valid
      end
    end

    context 'when lat value is invalid' do
      it 'the run should not accept wrong lat value' do
        run.lat = 100
        expect(run).to_not be_valid
      end
    end

    context 'when lng value is invalid' do
      it 'the run should not accept wrong lng value' do
        run.lng = 200
        expect(run).to_not be_valid
      end
    end

    context 'when time_sec value is invalid' do
      it 'the run should not accept wrong time_sec value' do
        run.time_sec = -1
        expect(run).to_not be_valid
      end
    end

    context 'when distance_m value is invalid' do
      it 'the run should not accept wrong distance_m value' do
        run.distance_m = 0
        expect(run).to_not be_valid
      end
    end

    context 'when weather value is invalid' do
      it 'the run should not accept wrong weather value' do
        run.weather = 'wrong'
        expect(run).to_not be_valid
      end
    end

    context 'when date value is invalid' do
      it 'the run should not accept wrong date value' do
        run.date = 'wrong date'
        expect(run).to_not be_valid
      end
    end

    context 'when lat and lng are not provided' do
      it 'the run should validate presence of location' do
        run.location = 'france'
        run.lat = nil
        run.lng = nil
        expect(run).to be_valid
      end

      it 'the run should validate presence of location' do
        run.location = nil
        run.lat = nil
        run.lng = nil
        expect(run).to_not be_valid
      end

      it 'the run should validate presence of lat and lng' do
        run.location = nil
        expect(run).to be_valid
      end

      it 'the run should validate presence of lat and lng' do
        run.location = nil
        run.lat = nil
        expect(run).not_to be_valid
      end

      it 'the run should validate presence of lat and lng' do
        run.location = nil
        run.lng = nil
        expect(run).not_to be_valid
      end
    end
  end

  context 'when run is created' do
    let(:run) { build :run }
    it 'should enqueue to RunWeatherSetter' do
      expect do
        run.save
      end.to change(Sidekiq::Queues['jogging::run-weather-setter'], :size).
        by(1)
    end
  end

  context 'associations' do
    it { expect(Run.reflect_on_association(:user).macro).to eq(:belongs_to) }
  end

  context 'speed calculation' do
    let(:run) { build :run }
    let(:expected) { run.distance_m / run.time_sec }

    it 'should calculate the speed correctly' do
      expect(run.speed_from_distance_and_time).to eq expected
    end
  end
end
