# typed: false
require 'rails_helper'

RSpec.describe RunWeatherSetter do
  let(:run) { create :run }
  let(:arg) { run.id }

  describe '.perform' do
    context 'when weatherapi returned a weather' do
      before do
        expect(Weatherapi).to receive(:find_weather_history_by).and_return([5, 200])
      end

      it 'should set the run weather' do
        RunWeatherSetter.new.perform(arg)

        expect(run.reload.weather).to eq 5
      end
    end

    context 'when weatherapi returned an error' do
      before do
        expect(Weatherapi).to receive(:find_weather_history_by).and_return([nil, 400])
      end

      it 'should not set the run weather' do
        RunWeatherSetter.new.perform(arg)

        expect(run.reload.weather).to be_nil
      end
    end

    context 'when providing none existent run id' do
      let(:arg) { 999 }

      it 'should not fail' do
        expect { RunWeatherSetter.new.perform(arg) }.not_to raise_error
      end
    end
  end
end
