require 'rails_helper'

describe UserPolicy do
  subject { described_class }
  let(:regular_user) { create :user, :regular_user }
  let(:other_regular_user) { create :user, :regular_user }
  let(:admin) { create :user, :admin }
  let(:user_manager) { create :user, :user_manager }

  permissions :create?, :destroy?, :update?, :index?, :show? do
    it "grants access for user manager" do
      expect(subject).to permit(user_manager, regular_user)
    end

    it "grants access for admin" do
      expect(subject).to permit(admin, regular_user)
    end

    it "denies access for user" do
      expect(subject).not_to permit(regular_user, other_regular_user)
    end
  end
end
