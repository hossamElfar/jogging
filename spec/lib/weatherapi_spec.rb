# typed: false
require 'rails_helper'

RSpec.describe Weatherapi do
  describe '.find_weather_history_by' do
    let(:params) do
      {
        lat: 48.8567,
        lng: 2.3508,
        date: Time.zone.now.to_date
      }
    end

    context 'recieved succesful response from weatherapi' do
      let(:expected_obj) do
        [3, 200]
      end

      before do
        WebMock.
          stub_request(:get, "https://api.weatherapi.com/v1/history.json?dt=#{params[:date]}&key=#{WEATHERAPI_API_KEY}&q=48.8567,2.3508").
          to_return(
            status: 200,
            body: "{\"forecast\":{\"forecastday\":[{\"date\":\"2021-01-15\",\"date_epoch\":1610668800,\"day\":{\"avgtemp_c\":3}}]}}",
            headers: { 'Content-type' => 'application/json' }
          )
      end

      it 'should return the correct weather' do
        expect(Weatherapi.find_weather_history_by(params)).to eq expected_obj
      end
    end

    context 'recieved none succesful response from weatherapi' do
      let(:expected_obj) do
        [nil, 400]
      end

      before do
        WebMock.
          stub_request(:get, "https://api.weatherapi.com/v1/history.json?dt=#{params[:date]}&key=#{WEATHERAPI_API_KEY}&q=48.8567,2.3508").
          to_return(
            status: 400,
            headers: { 'Content-type' => 'application/json' }
          )
      end

      it 'should return the correct weather' do
        expect(Weatherapi.find_weather_history_by(params)).to eq expected_obj
      end
    end

    context 'when lat anf lng were not provided' do
      context 'when location is provided' do
        let(:params) do
          {
            location: 'paris',
            date: Time.zone.now.to_date
          }
        end
        let(:expected_obj) do
          [3, 200]
        end

        before do
          WebMock.
            stub_request(:get, "https://api.weatherapi.com/v1/history.json?dt=#{params[:date]}&key=#{WEATHERAPI_API_KEY}&q=paris").
            to_return(
              status: 200,
              body: "{\"forecast\":{\"forecastday\":[{\"date\":\"2021-01-15\",\"date_epoch\":1610668800,\"day\":{\"avgtemp_c\":3}}]}}",
              headers: { 'Content-type' => 'application/json' }
            )
        end

        it 'should return the correct weather' do
          expect(Weatherapi.find_weather_history_by(params)).to eq expected_obj
        end
      end

      context 'when location is not provided' do
        let(:params) do
          {
            date: Time.zone.now.to_date
          }
        end

        let(:expected_obj) do
          [nil, -1]
        end

        it 'should not return the weather' do
          expect(Weatherapi.find_weather_history_by(params)).to eq expected_obj
        end
      end
    end

    context 'when date is not provided' do
      let(:params) do
        {
          location: 'paris'
        }
      end

      let(:expected_obj) do
        [nil, -1]
      end

      it 'should not return the weather' do
        expect(Weatherapi.find_weather_history_by(params)).to eq expected_obj
      end
    end
  end
end
