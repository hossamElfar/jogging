require 'rails_helper'

RSpec.describe UserDecorator do
  describe '.to_json' do
    let(:user) { create :user }
    let(:expected) do
      {
        id: user.id,
        name: user.name,
        email: user.email,
        role: user.role
      }
    end

    it 'should return the decorated user correctly' do
      result = user.decorate.to_json

      expect(result).to eq expected
    end
  end
end
