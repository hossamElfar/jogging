require 'rails_helper'

RSpec.describe RunDecorator do
  describe '.to_json' do
    let(:run) { create :run }
    let(:expected) do
      {
        id: run.id,
        location_data: {
          latitude: run.lat,
          longitude: run.lng,
          location: run.location
        },
        weather: run.weather,
        distance: run.distance_m,
        time: run.time_sec,
        speed: {
          value: run.speed.round(3),
          unit: 'm/s'
        },
        date: run.date
      }
    end

    it 'should return the decorated run correctly' do
      result = run.decorate.to_json

      expect(result).to eq expected
    end
  end
end
