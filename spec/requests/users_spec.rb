require 'rails_helper'

RSpec.describe "/user", type: :request do
  let(:user) { create :user }
  let(:actor) { create :user, :admin }

  let(:valid_attributes) do
    {
      name: Faker::Name.name,
      email: Faker::Internet.email,
      password: SecureRandom.uuid
    }
  end

  let(:invalid_attributes) do
    {
      password: '123'
    }
  end

  let(:valid_headers) do
    Devise::JWT::TestHelpers.auth_headers({}, actor)
  end
  let(:parsed_response) { JSON.parse(response.body).with_indifferent_access }

  before { Timecop.freeze(Time.zone.local(2021, 1, 15)) }
  after { Timecop.return }

  describe "GET /index" do
    let(:fixtures_path) { "#{Rails.root}/spec/fixtures/requests/users/index_request" }
    let(:read_file) { ->(file_name) { JSON.parse(File.read("#{fixtures_path}/#{file_name}.json")).with_indifferent_access } }

    context 'when the user is a regular user' do
      let(:actor) { create :user, :regular_user }
      let!(:users) { create_list :user, 8 }

      it 'should not return data' do
        get '/users', headers: valid_headers, as: :json

        expect(response).to have_http_status(:forbidden)
      end
    end

    context 'when the user is a user manager' do
      let(:actor) { create :user, :user_manager, email: 'actor@actor.com', name: 'zactor' }
      let(:expected) { read_file[file_name] }

      context 'pagination' do
        context 'when system has less than 8 users + 1 user_manager' do
          let!(:users) { create_list :user, 8 }
          let(:file_name) { 'pagination_less_than_10' }

          it 'should return all of them in the first page' do
            get '/users', headers: valid_headers, as: :json

            expect(response).to be_successful
            expect(parsed_response[:users].count).to eq 9
          end
        end

        context 'when the system has more than 10 users' do
          let!(:users) { create_list :user, 14 }

          context 'the first page' do
            let(:file_name) { 'pagination_more_than_10_1st_page' }

            it 'should return the first 10 only' do
              get '/users', headers: valid_headers, as: :json

              expect(response).to be_successful
              expect(parsed_response[:users].count).to eq 10
            end
          end

          context 'the second page' do
            let(:file_name) { 'pagination_more_than_10_2nd_page' }

            it 'should return the last 4 only' do
              get '/users?page=2', headers: valid_headers, as: :json

              expect(response).to be_successful
              expect(parsed_response[:users].count).to eq 5
            end
          end
        end
      end

      context 'filters' do
        let(:filters) { '' }

        context 'when no filters are given' do
          let!(:user) { create_list :user, 8 }
          let(:file_name) { 'no_filters' }

          it 'should return all users' do
            get '/users', headers: valid_headers, as: :json

            expect(response).to be_successful
            expect(parsed_response[:users].count).to eq 9
          end
        end

        context 'when 1 condition provided' do
          let!(:user_1) { create :user, email: 'awesome1@awesome1.com', name: 'awesome1' }
          let!(:user_2) { create :user, email: 'awesome2@awesome2.com', name: 'awesome2' }
          let(:file_name) { '1_condition' }
          let(:filters) { 'email eq awesome1@awesome1.com' }

          it 'should return 1 user' do
            get "/users?filters=#{filters}", headers: valid_headers, as: :json

            expect(response).to be_successful
            parsed_response[:users].each { |user| user[:id] = 1 }
            expect(parsed_response).to eq expected
          end
        end

        context 'when 2 conditions provided' do
          context 'when AND is the logical operator' do
            let!(:user_1) { create :user, email: 'awesome1@awesome1.com', name: 'awesome1' }
            let!(:user_2) { create :user, email: 'awesome2@awesome2.com', name: 'awesome1' }
            let!(:user_3) { create :user, email: 'awesome3@awesome3.com', name: 'awesome3' }
            let(:file_name) { '2_conditions_and' }
            let(:filters) { 'email eq awesome1@awesome1.com AND name eq awesome1' }

            it 'should return 1 users' do
              get "/users?filters=#{filters}", headers: valid_headers, as: :json

              expect(response).to be_successful
              parsed_response[:users].each { |user| user[:id] = 1 }
              expect(parsed_response).to eq expected
            end
          end

          context 'when OR is the logical operator' do
            let!(:user_1) { create :user, email: 'awesome1@awesome1.com', name: 'awesome1' }
            let!(:user_2) { create :user, email: 'awesome2@awesome2.com', name: 'awesome1' }
            let!(:user_3) { create :user, email: 'awesome3@awesome3.com', name: 'awesome3' }
            let!(:user_4) { create :user, email: 'awesome4@awesome4.com', name: 'awesome4' }

            let(:file_name) { '2_conditions_or' }
            let(:filters) { 'email eq awesome2@awesome2.com OR name eq awesome4' }

            it 'should return 2 users' do
              get "/users?filters=#{filters}", headers: valid_headers, as: :json

              expect(response).to be_successful
              parsed_response[:users].each { |user| user[:id] = 1 }
              expect(parsed_response).to eq expected
            end
          end
        end

        context 'when more than 3 conditions provided' do
          context 'when OR and AND are the logical operator' do
            let!(:user_1) { create :user, email: 'awesome1@awesome1.com', name: 'awesome1' }
            let!(:user_2) { create :user, email: 'awesome2@awesome2.com', name: 'awesome1' }
            let!(:user_3) { create :user, email: 'awesome3@awesome3.com', name: 'awesome3' }
            let!(:user_4) { create :user, email: 'awesome4@awesome4.com', name: 'awesome3' }
            let!(:user_5) { create :user, email: 'awesome5@awesome4.com', name: 'awesome3' }
            let!(:user_6) { create :user, email: 'awesome6@awesome4.com', name: 'awesome3' }
            let!(:user_7) { create :user, email: 'awesome7@awesome4.com', name: 'awesome7' }

            let(:file_name) { 'complix_conditions_and_or' }
            let(:filters) { '((name eq awesome3) AND ((email eq awesome4@awesome4.com) OR (email eq awesome3@awesome3.com)))' }

            it 'should return 2 users' do
              get "/users?filters=#{filters}", headers: valid_headers, as: :json

              expect(response).to be_successful
              parsed_response[:users].each { |user| user[:id] = 1 }
              expect(parsed_response).to eq expected
            end
          end
        end

        context 'when invalid filter provided' do
          context 'when filter by not supported attributes' do
            let(:user) { create :user }
            let(:filters) { 'dummy eq 3' }
            let(:file_name) { 'no_data' }

            it 'should not return data' do
              get "/users?filters=#{filters}", headers: valid_headers, as: :json

              expect(response).to be_successful
              parsed_response[:users].each { |user| user[:id] = 1 }
              expect(parsed_response).to eq expected
            end
          end

          context 'when the provided filter is malformed' do
            let(:user) { create :user }
            let(:filters) { 'email eq awesome@awesome.com AND (name eq kk' }

            it 'should return bad request' do
              get "/users?filters=#{filters}", headers: valid_headers, as: :json

              expect(response).to have_http_status(:bad_request)
            end
          end
        end
      end
    end

    context 'when the user is a user manager' do
      let(:actor) { create :user, :admin, email: 'actor@actor.com', name: 'zactor' }
      let(:expected) { read_file[file_name] }

      context 'pagination' do
        context 'when system has less than 8 users + 1 user_manager' do
          let!(:users) { create_list :user, 8 }
          let(:file_name) { 'pagination_less_than_10' }

          it 'should return all of them in the first page' do
            get '/users', headers: valid_headers, as: :json

            expect(response).to be_successful
            expect(parsed_response[:users].count).to eq 9
          end
        end

        context 'when the system has more than 10 users' do
          let!(:users) { create_list :user, 14 }

          context 'the first page' do
            let(:file_name) { 'pagination_more_than_10_1st_page' }

            it 'should return the first 10 only' do
              get '/users', headers: valid_headers, as: :json

              expect(response).to be_successful
              expect(parsed_response[:users].count).to eq 10
            end
          end

          context 'the second page' do
            let(:file_name) { 'pagination_more_than_10_2nd_page' }

            it 'should return the last 4 only' do
              get '/users?page=2', headers: valid_headers, as: :json

              expect(response).to be_successful
              expect(parsed_response[:users].count).to eq 5
            end
          end
        end
      end

      context 'filters' do
        let(:filters) { '' }

        context 'when no filters are given' do
          let!(:user) { create_list :user, 8 }
          let(:file_name) { 'no_filters' }

          it 'should return all users' do
            get '/users', headers: valid_headers, as: :json

            expect(response).to be_successful
            expect(parsed_response[:users].count).to eq 9
          end
        end

        context 'when 1 condition provided' do
          let!(:user_1) { create :user, email: 'awesome1@awesome1.com', name: 'awesome1' }
          let!(:user_2) { create :user, email: 'awesome2@awesome2.com', name: 'awesome2' }
          let(:file_name) { '1_condition' }
          let(:filters) { 'email eq awesome1@awesome1.com' }

          it 'should return 1 user' do
            get "/users?filters=#{filters}", headers: valid_headers, as: :json

            expect(response).to be_successful
            parsed_response[:users].each { |user| user[:id] = 1 }
            expect(parsed_response).to eq expected
          end
        end

        context 'when 2 conditions provided' do
          context 'when AND is the logical operator' do
            let!(:user_1) { create :user, email: 'awesome1@awesome1.com', name: 'awesome1' }
            let!(:user_2) { create :user, email: 'awesome2@awesome2.com', name: 'awesome1' }
            let!(:user_3) { create :user, email: 'awesome3@awesome3.com', name: 'awesome3' }
            let(:file_name) { '2_conditions_and' }
            let(:filters) { 'email eq awesome1@awesome1.com AND name eq awesome1' }

            it 'should return 1 users' do
              get "/users?filters=#{filters}", headers: valid_headers, as: :json

              expect(response).to be_successful
              parsed_response[:users].each { |user| user[:id] = 1 }
              expect(parsed_response).to eq expected
            end
          end

          context 'when OR is the logical operator' do
            let!(:user_1) { create :user, email: 'awesome1@awesome1.com', name: 'awesome1' }
            let!(:user_2) { create :user, email: 'awesome2@awesome2.com', name: 'awesome1' }
            let!(:user_3) { create :user, email: 'awesome3@awesome3.com', name: 'awesome3' }
            let!(:user_4) { create :user, email: 'awesome4@awesome4.com', name: 'awesome4' }

            let(:file_name) { '2_conditions_or' }
            let(:filters) { 'email eq awesome2@awesome2.com OR name eq awesome4' }

            it 'should return 2 users' do
              get "/users?filters=#{filters}", headers: valid_headers, as: :json

              expect(response).to be_successful
              parsed_response[:users].each { |user| user[:id] = 1 }
              expect(parsed_response).to eq expected
            end
          end
        end

        context 'when more than 3 conditions provided' do
          context 'when OR and AND are the logical operator' do
            let!(:user_1) { create :user, email: 'awesome1@awesome1.com', name: 'awesome1' }
            let!(:user_2) { create :user, email: 'awesome2@awesome2.com', name: 'awesome1' }
            let!(:user_3) { create :user, email: 'awesome3@awesome3.com', name: 'awesome3' }
            let!(:user_4) { create :user, email: 'awesome4@awesome4.com', name: 'awesome3' }
            let!(:user_5) { create :user, email: 'awesome5@awesome4.com', name: 'awesome3' }
            let!(:user_6) { create :user, email: 'awesome6@awesome4.com', name: 'awesome3' }
            let!(:user_7) { create :user, email: 'awesome7@awesome4.com', name: 'awesome7' }

            let(:file_name) { 'complix_conditions_and_or' }
            let(:filters) { '((name eq awesome3) AND ((email eq awesome4@awesome4.com) OR (email eq awesome3@awesome3.com)))' }

            it 'should return 2 users' do
              get "/users?filters=#{filters}", headers: valid_headers, as: :json

              expect(response).to be_successful
              parsed_response[:users].each { |user| user[:id] = 1 }
              expect(parsed_response).to eq expected
            end
          end
        end

        context 'when invalid filter provided' do
          context 'when filter by not supported attributes' do
            let(:user) { create :user }
            let(:filters) { 'dummy eq 3' }
            let(:file_name) { 'no_data' }

            it 'should not return data' do
              get "/users?filters=#{filters}", headers: valid_headers, as: :json

              expect(response).to be_successful
              parsed_response[:users].each { |user| user[:id] = 1 }
              expect(parsed_response).to eq expected
            end
          end

          context 'when the provided filter is malformed' do
            let(:user) { create :user }
            let(:filters) { 'email eq awesome@awesome.com AND (name eq kk' }

            it 'should return bad request' do
              get "/users?filters=#{filters}", headers: valid_headers, as: :json

              expect(response).to have_http_status(:bad_request)
            end
          end
        end
      end
    end
  end

  describe "GET /show" do
    context 'when user is a regular user' do
      let(:actor) { create :user, :regular_user }
      let(:user) { create :user }

      it "renders a none successful response" do
        get "/users/#{user.id}", headers: valid_headers, as: :json
        expect(response).to have_http_status(:forbidden)
      end
    end

    context 'when user is a user manager' do
      let(:actor) { create :user, :user_manager }

      context 'when requesting an existing user' do
        let(:user) { create :user, :regular_user }

        it "renders a successful response" do
          get "/users/#{user.id}", headers: valid_headers, as: :json
          expect(response).to be_successful
        end
      end

      context 'when requesting an none existing user' do
        it "renders a successful response" do
          get "/users/999", headers: valid_headers, as: :json
          expect(response).to have_http_status(:not_found)
        end
      end
    end

    context 'when user is an admin' do
      let(:actor) { create :user, :admin }

      context 'when requesting an existing user' do
        let(:user) { create :user, :regular_user }

        it "renders a successful response" do
          get "/users/#{user.id}", headers: valid_headers, as: :json
          expect(response).to be_successful
        end
      end

      context 'when requesting an none existing user' do
        it "renders a successful response" do
          get "/users/999", headers: valid_headers, as: :json
          expect(response).to have_http_status(:not_found)
        end
      end
    end
  end

  describe "POST /create" do
    context "with valid parameters" do
      context 'when the current user is a regular user' do
        let!(:actor) { create :user, :regular_user }

        it "should return forbidden" do
          post '/users',
               params: { user: valid_attributes }, headers: valid_headers, as: :json
          expect(response).to have_http_status(:forbidden)
        end

        it "should not create a new User" do
          expect do
            post '/users',
                 params: { user: valid_attributes }, headers: valid_headers, as: :json
          end.to change(User, :count).by(0)
        end
      end

      context 'when the current_user is admin' do
        let!(:actor) { create :user, :admin }

        it "renders a JSON response with the new user" do
          post '/users',
               params: { user: valid_attributes }, headers: valid_headers, as: :json
          expect(response).to have_http_status(:created)
        end

        it "creates a new User" do
          expect do
            post '/users',
                 params: { user: valid_attributes }, headers: valid_headers, as: :json
          end.to change(User, :count).by(1)
        end
      end

      context 'when the current_user is user manager' do
        let!(:actor) { create :user, :user_manager }

        it "renders a JSON response with the new user" do
          post '/users',
               params: { user: valid_attributes }, headers: valid_headers, as: :json
          expect(response).to have_http_status(:created)
        end

        it "creates a new User" do
          expect do
            post '/users',
                 params: { user: valid_attributes }, headers: valid_headers, as: :json
          end.to change(User, :count).by(1)
        end
      end
    end

    context "with invalid parameters" do
      let!(:actor) { create :user, :user_manager }

      it "does not create a new User" do
        expect do
          post '/users', headers: valid_headers,
               params: { user: invalid_attributes }, as: :json
        end.to change(User, :count).by(0)
      end

      it "renders a JSON response with errors for the new run" do
        post '/users',
             params: { user: invalid_attributes }, headers: valid_headers, as: :json
        expect(response).to have_http_status(:unprocessable_entity)
      end
    end
  end

  describe "PATCH /update" do
    context 'when user is a regular user' do
      let(:actor) { create :user, :regular_user }
      let(:regular_user) { create :user, :regular_user }
      let(:new_attributes) do
        { name: 'awesome' }
      end

      it "should return forbidden status code" do
        patch "/users/#{user.id}",
              params: { user: new_attributes }, headers: valid_headers, as: :json

        expect(response).to have_http_status(:forbidden)
      end
    end

    context 'when user is admin' do
      context "with valid parameters" do
        let(:actor) { create :user, :admin }
        let(:user) { create :user, :regular_user }

        context 'when updating the values of a user' do
          let(:new_attributes) do
            { name: 'awesome' }
          end

          it "updates the requested user" do
            patch "/users/#{user.id}",
                  params: { user: new_attributes }, headers: valid_headers, as: :json
            expect(response).to be_successful
          end

          it "renders a JSON response with the user" do
            patch "/users/#{user.id}",
                  params: { user: new_attributes }, headers: valid_headers, as: :json
            expect(response).to have_http_status(:ok)
            expect(user.reload.name).to eq new_attributes[:name]
          end
        end

        context 'when updating the role of a regular user' do
          let(:new_attributes) do
            { role: :admin }
          end

          it "updates the requested user" do
            patch "/users/#{user.id}",
                  params: { user: new_attributes }, headers: valid_headers, as: :json
            expect(response).to be_successful
          end

          it "renders a JSON response with the user" do
            patch "/users/#{user.id}",
                  params: { user: new_attributes }, headers: valid_headers, as: :json
            expect(response).to have_http_status(:ok)
            expect(user.reload.admin?).to eq true
          end
        end

        context 'when updating the values of a user manager' do
          let(:actor) { create :user, :admin }
          let(:user) { create :user, :user_manager }

          context 'when updating the values of a user' do
            let(:new_attributes) do
              { role: :user }
            end

            it "updates the requested user" do
              patch "/users/#{user.id}",
                    params: { user: new_attributes }, headers: valid_headers, as: :json
              expect(response).to be_successful
            end

            it "renders a JSON response with the user" do
              patch "/users/#{user.id}",
                    params: { user: new_attributes }, headers: valid_headers, as: :json
              expect(response).to have_http_status(:ok)
              expect(user.reload.user?).to eq true
            end
          end
        end

        context "with invalid parameters" do
          let!(:user) { create :user }

          it "renders a JSON response with errors for the user" do
            patch "/users/#{user.id}",
                  params: { run: invalid_attributes }, headers: valid_headers, as: :json
            expect(response).to have_http_status(:unprocessable_entity)
          end
        end
      end
    end

    context 'when user is a user manager' do
      context "with valid parameters" do
        let(:actor) { create :user, :user_manager }
        let(:user) { create :user, :regular_user }

        context 'when updating the values of a user' do
          let(:new_attributes) do
            { name: 'awesome' }
          end

          it "updates the requested user" do
            patch "/users/#{user.id}",
                  params: { user: new_attributes }, headers: valid_headers, as: :json
            expect(response).to be_successful
          end

          it "renders a JSON response with the user" do
            patch "/users/#{user.id}",
                  params: { user: new_attributes }, headers: valid_headers, as: :json
            expect(response).to have_http_status(:ok)
            expect(user.reload.name).to eq new_attributes[:name]
          end
        end

        context 'when changing the role of a user to an admin' do
          let(:new_attributes) do
            { role: :admin }
          end

          it "updates the requested user" do
            patch "/users/#{user.id}",
                  params: { user: new_attributes }, headers: valid_headers, as: :json
            expect(response).to have_http_status(:forbidden)
          end
        end
      end

      context "with invalid parameters" do
        let!(:user) { create :user }

        it "renders a JSON response with errors for the user" do
          patch "/users/#{user.id}",
                params: { run: invalid_attributes }, headers: valid_headers, as: :json
          expect(response).to have_http_status(:unprocessable_entity)
        end
      end

      context 'when updating the values of an admin' do
        let(:actor) { create :user, :user_manager }
        let(:user) { create :user, :admin }

        context 'when updating the role for an admin' do
          let(:new_attributes) do
            { role: :user }
          end

          it "should not update the requested user" do
            patch "/users/#{user.id}",
                  params: { user: new_attributes }, headers: valid_headers, as: :json
            expect(response).to have_http_status(:forbidden)
          end
        end
      end
    end
  end

  describe "DELETE /destroy" do
    let!(:user) { create :user }

    context 'when the user is a regular user' do
      let!(:actor) { create :user, :regular_user }

      it 'should not destroy that run' do
        delete "/users/#{user.id}", headers: valid_headers, as: :json

        expect(response).to have_http_status(:forbidden)
      end
    end

    context 'when the user is an admin' do
      let!(:actor) { create :user, :admin }

      context 'when trying to destroy a user' do
        it "destroys the requested user" do
          expect do
            delete "/users/#{user.id}", headers: valid_headers, as: :json
          end.to change(User, :count).by(-1)
        end
      end

      context 'when trying to delete a user manager' do
        let!(:user) { create :user, :user_manager }

        it "should not destroy the requested user" do
          delete "/users/#{user.id}", headers: valid_headers, as: :json
          expect(response).to have_http_status(:no_content)
        end

        it "should not destroy the requested user" do
          expect do
            delete "/users/#{user.id}", headers: valid_headers, as: :json
          end.to change(User, :count).by(-1)
        end
      end
    end

    context 'when the user is a user manager' do
      let!(:actor) { create :user, :user_manager }
      let!(:user) { create :user, :admin }

      context 'when trying to destroy an admin' do
        it "should not destroy the requested user" do
          delete "/users/#{user.id}", headers: valid_headers, as: :json
          expect(response).to have_http_status(:forbidden)
        end

        it "should not destroy the requested user" do
          expect do
            delete "/users/#{user.id}", headers: valid_headers, as: :json
          end.to change(User, :count).by(0)
        end
      end
    end
  end
end
