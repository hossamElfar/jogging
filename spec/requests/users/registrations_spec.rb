require 'rails_helper'

RSpec.describe "Users::Registrations", type: :request do
  describe "POST /auth/users" do
    let(:name) { Faker::Name.name }
    let(:email) { Faker::Internet.email }
    let(:password) { SecureRandom.uuid }
    let(:password_confirmation) { password }
    let(:params) do
      {
        user: {
          name: name,
          email: email,
          password: password,
          password_confirmation: password_confirmation
        }
      }
    end
    let(:parsed_response) { JSON.parse(response.body, symbolize_names: true) }

    context 'when user enter valid params' do
      it 'should sign up successfuly and return the jwt token' do
        post '/auth/users', params: params, as: :json

        expect(response).to have_http_status(200)
        expect(response.headers['Authorization']).not_to be_blank
      end
    end

    context 'when user enters invalid data' do
      context 'when email is already taken' do
        let!(:user) { create :user }
        let(:email) { user.email }

        it 'should not sign up' do
          post '/auth/users', params: params, as: :json

          expect(response).to have_http_status(422)
        end
      end

      context 'when password is too short' do
        let(:password) { '123' }

        it 'should not sign up' do
          post '/auth/users', params: params, as: :json

          expect(response).to have_http_status(422)
        end
      end

      context 'when password and password confirmation does not match' do
        let(:password_confirmation) { 'wrong' }

        it 'should not sign up' do
          post '/auth/users', params: params, as: :json

          expect(response).to have_http_status(422)
        end
      end
    end
  end

  describe "DELETE /auth/users" do
    let(:user) { create :user }
    let(:headers) { {} }

    context 'when user provides valid auth headers' do
      let!(:headers) { Devise::JWT::TestHelpers.auth_headers({}, user) }

      it 'should delete the user record' do
        delete '/auth/users', headers: headers, as: :json

        expect(response).to have_http_status(200)
        expect do
          user.reload
        end.to raise_error(ActiveRecord::RecordNotFound)
      end

      it 'should delete the record from the db' do
        expect do
          delete '/auth/users', headers: headers, as: :json
        end.to change { User.count }.from(1).to(0)
      end
    end

    context 'when user provides wrong authorization token' do
      it 'should not change the User' do
        expect do
          delete '/auth/users', headers: headers, as: :json
        end.to change { User.count }.by(0)
      end
    end
  end

  describe "PATCH /auth/users" do
    let(:user) { create :user }
    let(:headers) { {} }
    let(:name) { 'awesome' }
    let(:params) do
      {
        user: {
          name: name
        }
      }
    end

    context 'when user provides valid auth headers' do
      let!(:headers) { Devise::JWT::TestHelpers.auth_headers({}, user) }

      it 'should update the user record' do
        patch '/auth/users', params: params, headers: headers, as: :json

        expect(response).to have_http_status(200)
        expect(user.reload.name).to eq name
      end
    end

    context 'when user provides empty params' do
      let!(:headers) { Devise::JWT::TestHelpers.auth_headers({}, user) }
      let(:params) { {} }

      it 'should update the user record' do
        patch '/auth/users', params: params, headers: headers, as: :json

        expect(response).to have_http_status(400)
      end
    end

    context 'when user provides wrong authorization token' do
      it 'should not update the user info' do
        patch '/auth/users', headers: headers, as: :json

        expect(response).to have_http_status(401)
      end
    end
  end
end
