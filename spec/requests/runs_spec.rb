require 'rails_helper'

RSpec.describe "/users/:user_id/runs", type: :request do
  let(:user) { create :user }
  let(:valid_attributes) do
    {
      lat: Faker::Address.latitude,
      lng: Faker::Address.longitude,
      date: Time.zone.now,
      time_sec: Faker::Number.between(from: 1, to: 10),
      distance_m: Faker::Number.between(from: 1, to: 10)
    }
  end

  let(:invalid_attributes) do
    {
      lat: 1000,
      lng: 1000,
      date: Time.zone.now,
      time_sec: Faker::Number.between(from: 1, to: 10),
      distance_m: Faker::Number.between(from: 1, to: 10)
    }
  end

  let(:valid_headers) do
    Devise::JWT::TestHelpers.auth_headers({}, user)
  end
  let(:parsed_response) { JSON.parse(response.body).with_indifferent_access }

  before { Timecop.freeze(Time.zone.local(2021, 1, 15)) }
  after { Timecop.return }

  describe "GET /index" do
    let(:fixtures_path) { "#{Rails.root}/spec/fixtures/requests/runs/index_request" }
    let(:read_file) { ->(file_name) { JSON.parse(File.read("#{fixtures_path}/#{file_name}.json")).with_indifferent_access } }

    context 'pagination' do
      context 'when the user has less than 10 runs' do
        let!(:runs) { create_list :run, 8, user: user, distance_m: 400, time_sec: 1200, weather: 20 }
        let(:file_name) { 'pagination_less_than_10' }
        let(:expected) { read_file[file_name] }

        it 'should return all of them in the first page' do
          get "/users/#{user.id}/runs", headers: valid_headers, as: :json

          expect(response).to be_successful
          parsed_response[:runs].each { |run| run[:id] = 1 }
          expect(parsed_response).to eq expected
        end
      end

      context 'when the user has more than 10 runs' do
        let!(:runs) { create_list :run, 14, user: user, distance_m: 400, time_sec: 1200, weather: 20 }
        let(:expected) { read_file[file_name] }

        context 'the first page' do
          let(:file_name) { 'pagination_more_than_10_1st_page' }

          it 'should retuen the first 10 only' do
            get "/users/#{user.id}/runs", headers: valid_headers, as: :json

            expect(response).to be_successful
            parsed_response[:runs].each { |run| run[:id] = 1 }
            expect(parsed_response).to eq expected
          end
        end

        context 'wthe second page' do
          let(:file_name) { 'pagination_more_than_10_2nd_page' }

          it 'should retuen the last 4 only' do
            get "/users/#{user.id}/runs?page=2", headers: valid_headers, as: :json

            expect(response).to be_successful
            parsed_response[:runs].each { |run| run[:id] = 1 }
            expect(parsed_response).to eq expected
          end
        end
      end
    end

    context 'filters' do
      let(:filters) { '' }
      let(:expected) { read_file[file_name] }

      context 'when no filters are given' do
        let!(:runs) { create_list :run, 8, user: user, distance_m: 400, time_sec: 1200, weather: 20 }
        let(:file_name) { 'no_filters' }

        it 'should return all of them' do
          get "/users/#{user.id}/runs?filters=#{filters}", headers: valid_headers, as: :json

          expect(response).to be_successful
          parsed_response[:runs].each { |run| run[:id] = 1 }
          expect(parsed_response).to eq expected
        end
      end

      context 'when 1 condition provided' do
        let!(:run_1) { create :run, user: user, distance_m: 1500, time_sec: 600 }
        let!(:run_2) { create :run, user: user, distance_m: 500, time_sec: 300 }
        let(:file_name) { '1_condition' }
        let(:filters) { 'distance gt 1000' }

        it 'should return 1 run' do
          get "/users/#{user.id}/runs?filters=#{filters}", headers: valid_headers, as: :json

          expect(response).to be_successful
          parsed_response[:runs].each { |run| run[:id] = 1 }
          expect(parsed_response).to eq expected
        end
      end

      context 'when 2 conditions provided' do
        context 'when AND is the logical operator' do
          let!(:run_1) { create :run, user: user, distance_m: 1500, time_sec: 1200 }
          let!(:run_2) { create :run, user: user, distance_m: 500, time_sec: 1100 }
          let!(:run_3) { create :run, user: user, distance_m: 500, time_sec: 900 }
          let(:file_name) { '2_conditions_and' }
          let(:filters) { 'distance gt 400 AND time gt 1000' }

          it 'should return 2 runs' do
            get "/users/#{user.id}/runs?filters=#{filters}", headers: valid_headers, as: :json

            expect(response).to be_successful
            parsed_response[:runs].each { |run| run[:id] = 1 }
            expect(parsed_response).to eq expected
          end
        end

        context 'when OR is the logical operator' do
          let!(:run_1) { create :run, user: user, distance_m: 1500, time_sec: 1200 }
          let!(:run_2) { create :run, user: user, distance_m: 500, time_sec: 1100 }
          let!(:run_3) { create :run, user: user, distance_m: 200, time_sec: 900 }
          let!(:run_4) { create :run, user: user, distance_m: 200, time_sec: 600 }
          let(:file_name) { '2_conditions_or' }
          let(:filters) { 'distance gt 300 OR time gt 800' }

          it 'should return 3 runs' do
            get "/users/#{user.id}/runs?filters=#{filters}", headers: valid_headers, as: :json

            expect(response).to be_successful
            parsed_response[:runs].each { |run| run[:id] = 1 }
            expect(parsed_response).to eq expected
          end
        end
      end

      context 'when 3 conditions provided' do
        context 'when AND is the logical operator' do
          let!(:run_1) { create :run, user: user, distance_m: 1500, time_sec: 1200, location: 'loc1' }
          let!(:run_2) { create :run, user: user, distance_m: 500, time_sec: 1100, location: 'loc1' }
          let!(:run_3) { create :run, user: user, distance_m: 200, time_sec: 900, location: 'loc2' }
          let!(:run_4) { create :run, user: user, distance_m: 100, time_sec: 600, location: 'loc3' }
          let(:file_name) { '3_conditions_and' }
          let(:filters) { 'distance gt 100 AND time gt 800 AND location eq loc1' }

          it 'should return 2 runs' do
            get "/users/#{user.id}/runs?filters=#{filters}", headers: valid_headers, as: :json

            expect(response).to be_successful
            parsed_response[:runs].each { |run| run[:id] = 1 }
            expect(parsed_response).to eq expected
          end
        end

        context 'when OR is the logical operator' do
          let!(:run_1) { create :run, user: user, distance_m: 1500, time_sec: 1200, location: 'loc1' }
          let!(:run_2) { create :run, user: user, distance_m: 500, time_sec: 1100, location: 'loc1' }
          let!(:run_3) { create :run, user: user, distance_m: 200, time_sec: 900, location: 'loc2' }
          let!(:run_4) { create :run, user: user, distance_m: 100, time_sec: 600, location: 'loc3' }
          let(:file_name) { '3_conditions_or' }
          let(:filters) { 'distance gt 100 OR time gt 800 OR location eq loc1' }

          it 'should return 3 runs' do
            get "/users/#{user.id}/runs?filters=#{filters}", headers: valid_headers, as: :json

            expect(response).to be_successful
            parsed_response[:runs].each { |run| run[:id] = 1 }
            expect(parsed_response).to eq expected
          end
        end

        context 'when OR and AND are the logical operator' do
          let!(:run_1) { create :run, user: user, distance_m: 1500, time_sec: 1300, location: 'loc1' }
          let!(:run_2) { create :run, user: user, distance_m: 500, time_sec: 1100, location: 'loc1' }
          let!(:run_3) { create :run, user: user, distance_m: 200, time_sec: 900, location: 'loc2' }
          let!(:run_4) { create :run, user: user, distance_m: 100, time_sec: 600, location: 'loc3' }
          let(:file_name) { '3_conditions_and_or' }
          let(:filters) { '((distance gt 1000) OR (time gt 1100)) AND (location eq loc1)' }

          it 'should return 1 run' do
            get "/users/#{user.id}/runs?filters=#{filters}", headers: valid_headers, as: :json

            expect(response).to be_successful
            parsed_response[:runs].each { |run| run[:id] = 1 }
            expect(parsed_response).to eq expected
          end
        end
      end

      context 'when more than 3 conditions provided' do
        context 'when OR and AND are the logical operator' do
          let!(:run_1) { create :run, user: user, distance_m: 1500, time_sec: 1300, location: 'loc1' }
          let!(:run_2) { create :run, user: user, distance_m: 500, time_sec: 1100, location: 'loc1' }
          let!(:run_3) { create :run, user: user, distance_m: 200, time_sec: 900, location: 'loc2' }
          let!(:run_4) { create :run, user: user, distance_m: 2000, time_sec: 3600, location: 'loc4' }
          let!(:run_5) { create :run, user: user, distance_m: 1800, time_sec: 1600, location: 'loc1' }
          let!(:run_6) { create :run, user: user, distance_m: 1850, time_sec: 2600, location: 'loc1' }

          let(:file_name) { 'complix_conditions_and_or' }
          let(:filters) { '((location eq loc1) AND (((time gt 1000) AND (distance lt 1850)) OR ((time lt 1000) AND (distance gt 500))))' }

          it 'should return 3 run' do
            get "/users/#{user.id}/runs?filters=#{filters}", headers: valid_headers, as: :json

            expect(response).to be_successful
            parsed_response[:runs].each { |run| run[:id] = 1 }
            expect(parsed_response).to eq expected
          end
        end
      end

      context 'when invalid filter provided' do
        context 'when filter by not supported attributes' do
          let!(:run_1) { create :run, user: user, distance_m: 1500, time_sec: 600 }
          let(:filters) { 'dummy eq 3' }
          let(:file_name) { 'no_data' }

          it 'should not return data' do
            get "/users/#{user.id}/runs?filters=#{filters}", headers: valid_headers, as: :json

            expect(response).to be_successful
            parsed_response[:runs].each { |run| run[:id] = 1 }
            expect(parsed_response).to eq expected
          end
        end

        context 'when the provided filter is malformed' do
          let!(:run_1) { create :run, user: user, distance_m: 1500, time_sec: 600 }
          let(:filters) { 'distance eq 100 AND (time lt 90' }

          it 'should return bad request' do
            get "/users/#{user.id}/runs?filters=#{filters}", headers: valid_headers, as: :json

            expect(response).to have_http_status(:bad_request)
          end
        end
      end
    end

    context 'when the user is a user manager' do
      let(:user) { create :user, :user_manager }
      let(:regular_user) { create :user, :regular_user }
      let!(:runs) { create_list :run, 8, user: regular_user, distance_m: 400, time_sec: 1200, weather: 20 }

      it 'should not return data' do
        get "/users/#{user.id}/runs", headers: valid_headers, as: :json

        expect(response).to have_http_status(:forbidden)
      end
    end

    context 'when the user is an admin' do
      context 'when user id is provided' do
        let(:user) { create :user, :admin }
        let(:regular_user_1) { create :user, :regular_user }
        let(:regular_user_2) { create :user, :regular_user }
        let!(:user_1_runs) { create_list :run, 4, user: regular_user_1, distance_m: 400, time_sec: 1200, weather: 20 }
        let!(:user_2_runs) { create_list :run, 4, user: regular_user_2, distance_m: 400, time_sec: 1200, weather: 20 }

        it 'should return user 1 data only' do
          get "/users/#{regular_user_1.id}/runs", headers: valid_headers, as: :json

          expect(response).to have_http_status(:ok)
          expect(parsed_response[:runs].count).to eq 4
        end
      end
    end
  end

  describe "GET /show" do
    context 'when user is a regular user' do
      context 'when requesting an existing run' do
        let(:run) { create :run, user: user }

        it "renders a successful response" do
          get "/users/#{user.id}/runs/#{run.id}", headers: valid_headers, as: :json
          expect(response).to be_successful
        end
      end

      context 'when requesting an none existing run' do
        it "renders a successful response" do
          get "/users/#{user.id}/runs/999", headers: valid_headers, as: :json
          expect(response).to have_http_status(:not_found)
        end
      end

      context 'when requesting an run from other user' do
        let(:my_run) { create :run, user: user }
        let(:other_user_run) { create :run }

        it "renders a successful response" do
          get "/users/#{user.id}/runs/#{other_user_run.id}", headers: valid_headers, as: :json
          expect(response).to have_http_status(:not_found)
        end
      end
    end

    context 'when user is a user manager' do
      let(:user) { create :user, :user_manager }
      let(:run) { create :run }

      it "renders a forbidden response" do
        get "/users/#{user.id}/runs/#{run.id}", headers: valid_headers, as: :json
        expect(response).to have_http_status(:forbidden)
      end
    end

    context 'when user is an admin' do
      context 'when requesting an existing run' do
        let(:user) { create :user, :admin }
        let(:regular_user) { create :user, :regular_user }
        let(:run) { create :run, user: regular_user }

        it "renders a successful response" do
          get "/users/#{regular_user.id}/runs/#{run.id}", headers: valid_headers, as: :json
          expect(response).to be_successful
        end
      end

      context 'when requesting an none existing run' do
        let(:user) { create :user, :admin }
        let(:regular_user) { create :user, :regular_user }

        it "renders a successful response" do
          get "/users/#{regular_user.id}/runs/9999", headers: valid_headers, as: :json
          expect(response).to have_http_status(:not_found)
        end
      end

      context 'when requesting an run from other user' do
        let(:user) { create :user, :admin }
        let(:regular_user) { create :user, :regular_user }
        let(:run) { create :run, user: regular_user }

        it "renders a successful response" do
          get "/users/#{regular_user.id}/runs/#{run.id}", headers: valid_headers, as: :json
          expect(response).not_to have_http_status(:forbidden)
        end
      end
    end
  end

  describe "GET /report" do
    let(:fixtures_path) { "#{Rails.root}/spec/fixtures/requests/runs/report_request" }
    let(:read_file) { ->(file_name) { JSON.parse(File.read("#{fixtures_path}/#{file_name}.json")).with_indifferent_access } }

    context 'when user has data in 1 week' do
      let!(:runs) { create_list :run, 8, user: user, distance_m: 400, time_sec: 1200, weather: 20, date: 1.week.ago }
      let(:file_name) { 'report_1_week' }
      let(:expected) { read_file[file_name] }

      it 'should return the correct week data' do
        get "/users/#{user.id}/runs/report", headers: valid_headers, as: :json

        expect(response).to be_successful
        expect(parsed_response).to eq expected
      end
    end

    context 'when user has data in 3 consecutive weeks' do
      let!(:runs_1st_week) { create_list :run, 8, user: user, distance_m: 400, time_sec: 1200, weather: 20, date: 1.week.ago }
      let!(:runs_2nd_week) { create_list :run, 8, user: user, distance_m: 400, time_sec: 1200, weather: 20, date: 2.weeks.ago }
      let!(:runs_3rd_week) { create_list :run, 8, user: user, distance_m: 400, time_sec: 1200, weather: 20, date: 3.weeks.ago }
      let(:file_name) { 'report_3_weeks' }
      let(:expected) { read_file[file_name] }

      it 'should return the correct weeks data' do
        get "/users/#{user.id}/runs/report", headers: valid_headers, as: :json

        expect(response).to be_successful
        expect(parsed_response).to eq expected
      end
    end

    context 'when user has data in 6 none connected weeks' do
      let!(:runs_1st_week) { create_list :run, 8, user: user, distance_m: 400, time_sec: 1200, weather: 20, date: 1.week.ago }
      let!(:runs_2nd_week) { create_list :run, 8, user: user, distance_m: 400, time_sec: 1200, weather: 20, date: 3.weeks.ago }
      let!(:runs_3rd_week) { create_list :run, 8, user: user, distance_m: 400, time_sec: 1200, weather: 20, date: 5.weeks.ago }
      let!(:runs_4th_week) { create_list :run, 8, user: user, distance_m: 400, time_sec: 1200, weather: 20, date: 7.weeks.ago }
      let!(:runs_5th_week) { create_list :run, 8, user: user, distance_m: 400, time_sec: 1200, weather: 20, date: 10.weeks.ago }
      let!(:runs_6th_week) { create_list :run, 8, user: user, distance_m: 400, time_sec: 1200, weather: 20, date: 30.weeks.ago }
      let(:file_name) { 'report_6_weeks' }
      let(:expected) { read_file[file_name] }

      it 'should return the correct weeks data' do
        get "/users/#{user.id}/runs/report", headers: valid_headers, as: :json

        expect(response).to be_successful
        expect(parsed_response).to eq expected
      end
    end

    context 'filters' do
      context 'when filtering by 1 condition' do
        let!(:runs_1) { create_list :run, 8, user: user, distance_m: 400, time_sec: 1200, weather: 20, date: 1.week.ago }
        let!(:runs_2) { create_list :run, 8, user: user, distance_m: 600, time_sec: 1400, weather: 20, date: 2.weeks.ago }
        let!(:runs_3) { create_list :run, 8, user: user, distance_m: 800, time_sec: 1600, weather: 20, date: 3.weeks.ago }
        let(:filters) { 'distance gt 500' }

        let(:file_name) { 'report_filters_1_condition' }
        let(:expected) { read_file[file_name] }

        it 'should return the correct week data' do
          get "/users/#{user.id}/runs/report?filters=#{filters}", headers: valid_headers, as: :json

          expect(response).to be_successful
          expect(parsed_response).to eq expected
        end
      end

      context 'when filtering by 2 conditions' do
        let!(:runs_1) { create_list :run, 8, user: user, distance_m: 400, time_sec: 1200, weather: 20, date: 1.week.ago }
        let!(:runs_2) { create_list :run, 8, user: user, distance_m: 600, time_sec: 1400, weather: 20, date: 1.week.ago }
        let!(:runs_3) { create_list :run, 8, user: user, distance_m: 800, time_sec: 1600, weather: 20, date: 2.weeks.ago }
        let!(:runs_4) { create_list :run, 8, user: user, distance_m: 800, time_sec: 1600, weather: 20, date: 3.weeks.ago }
        let!(:runs_5) { create_list :run, 8, user: user, distance_m: 800, time_sec: 1600, weather: 20, date: 4.weeks.ago }

        let(:filters) { "date gt #{4.weeks.ago.to_date} AND date lt #{2.weeks.ago.to_date}" }

        let(:file_name) { 'report_filters_2_conditions' }
        let(:expected) { read_file[file_name] }

        it 'should return the correct week data' do
          get "/users/#{user.id}/runs/report?filters=#{filters}", headers: valid_headers, as: :json

          expect(response).to be_successful
          expect(parsed_response).to eq expected
        end
      end

      context 'when filtering by 3 conditions' do
        let!(:runs_1) { create_list :run, 8, user: user, distance_m: 400, time_sec: 1200, weather: 20, date: 1.week.ago }
        let!(:runs_2) { create_list :run, 8, user: user, distance_m: 600, time_sec: 1400, weather: 20, date: 1.week.ago }
        let!(:runs_3) { create_list :run, 8, user: user, distance_m: 800, time_sec: 1600, weather: 20, date: 2.weeks.ago }
        let!(:runs_4) { create_list :run, 8, user: user, distance_m: 800, time_sec: 1600, weather: 20, date: 3.weeks.ago }
        let!(:runs_5) { create_list :run, 8, user: user, distance_m: 900, time_sec: 1600, weather: 20, date: 3.weeks.ago }
        let!(:runs_6) { create_list :run, 8, user: user, distance_m: 800, time_sec: 1600, weather: 20, date: 4.weeks.ago }

        let(:filters) { "date gt #{4.weeks.ago.to_date} AND date lt #{2.weeks.ago.to_date} AND distance eq 900" }

        let(:file_name) { 'report_filters_3_conditions' }
        let(:expected) { read_file[file_name] }

        it 'should return the correct week data' do
          get "/users/#{user.id}/runs/report?filters=#{filters}", headers: valid_headers, as: :json

          expect(response).to be_successful
          expect(parsed_response).to eq expected
        end
      end
    end

    context 'pagination' do
      let!(:runs_1st_week) { create_list :run, 8, user: user, distance_m: 400, time_sec: 1200, weather: 20, date: 1.week.ago }
      let!(:runs_2nd_week) { create_list :run, 8, user: user, distance_m: 400, time_sec: 1200, weather: 20, date: 3.weeks.ago }
      let!(:runs_3rd_week) { create_list :run, 8, user: user, distance_m: 400, time_sec: 1200, weather: 20, date: 5.weeks.ago }
      let!(:runs_4th_week) { create_list :run, 8, user: user, distance_m: 400, time_sec: 1200, weather: 20, date: 7.weeks.ago }
      let!(:runs_4th_week) { create_list :run, 8, user: user, distance_m: 400, time_sec: 1200, weather: 20, date: 11.weeks.ago }
      let(:expected) { read_file[file_name] }

      context 'when requesting from the first week' do
        let(:file_name) { 'report_pagination_1st_page' }

        it 'should return the correct weeks data' do
          get "/users/#{user.id}/runs/report", headers: valid_headers, as: :json

          expect(response).to be_successful
          expect(parsed_response).to eq expected
        end
      end

      context 'when requesting from the 2nd page' do
        let(:file_name) { 'report_pagination_2nd_page' }

        it 'should return the correct weeks data' do
          get "/users/#{user.id}/runs/report?page=2", headers: valid_headers, as: :json

          expect(response).to be_successful
          expect(parsed_response).to eq expected
        end
      end
    end

    context 'when requesting the report as admin' do
      let(:user) { create :user, :admin }
      let(:regular_user) { create :user, :regular_user }
      let!(:runs) { create_list :run, 8, user: regular_user, distance_m: 400, time_sec: 1200, weather: 20, date: 1.week.ago }
      let(:file_name) { 'report_1_week' }
      let(:expected) { read_file[file_name] }

      context 'when providing the user id as param' do
        it 'should return the correct week data' do
          get "/users/#{regular_user.id}/runs/report", headers: valid_headers, as: :json

          expect(response).to be_successful
          expect(parsed_response).to eq expected
        end
      end
    end

    context 'when requesting the report as a user manager' do
      let(:user) { create :user, :user_manager }
      let(:regular_user) { create :user, :regular_user }
      let!(:runs) { create_list :run, 8, user: regular_user, distance_m: 400, time_sec: 1200, weather: 20, date: 1.week.ago }

      it 'should not return the data' do
        get "/users/#{regular_user.id}/runs/report", headers: valid_headers, as: :json

        expect(response).to have_http_status(:forbidden)
      end
    end
  end

  describe "POST /create" do
    context "with valid parameters" do
      context 'when the current use is a regular user' do
        it "creates a new Run" do
          expect do
            post "/users/#{user.id}/runs",
                 params: { run: valid_attributes }, headers: valid_headers, as: :json
          end.to change(user.runs, :count).by(1)
        end

        it "renders a JSON response with the new run" do
          post "/users/#{user.id}/runs",
               params: { run: valid_attributes }, headers: valid_headers, as: :json
          expect(response).to have_http_status(:created)
        end
      end

      context 'when the current_user is admin' do
        let(:user) { create :user, :admin }
        let(:regular_user) { create :user, :regular_user }

        it "should retun unprocessable_entity if user_id is missing" do
          post "/users/#{regular_user.id}/runs",
               params: { run: invalid_attributes }, headers: valid_headers, as: :json
          expect(response).to have_http_status(:unprocessable_entity)
        end

        it "renders a JSON response with the new run" do
          post "/users/#{regular_user.id}/runs",
               params: { run: valid_attributes }, headers: valid_headers, as: :json
          expect(response).to have_http_status(:created)
        end

        it "creates a new Run and associate it to the right user" do
          expect do
            post "/users/#{regular_user.id}/runs",
                 params: { run: valid_attributes }, headers: valid_headers, as: :json
          end.to change(regular_user.runs, :count).by(1)
        end

        it "creates a new Run and not associate it to the admin" do
          expect do
            post "/users/#{regular_user.id}/runs",
                 params: { run: valid_attributes }, headers: valid_headers, as: :json
          end.to change(user.runs, :count).by(0)
        end
      end

      context 'when the current_user is user manager' do
        let(:user) { create :user, :user_manager }
        let(:regular_user) { create :user, :regular_user }

        it "should return forbidden" do
          post "/users/#{regular_user.id}/runs",
               params: { run: valid_attributes }, headers: valid_headers, as: :json
          expect(response).to have_http_status(:forbidden)
        end

        it "should not create a new Run and not associate it to the user" do
          expect do
            post "/users/#{regular_user.id}/runs",
                 params: { run: valid_attributes }, headers: valid_headers, as: :json
          end.to change(regular_user.runs, :count).by(0)
        end

        it "should not creates a new Run and not associate it to the user manager" do
          expect do
            post "/users/#{regular_user.id}/runs",
                 params: { run: valid_attributes }, headers: valid_headers, as: :json
          end.to change(user.runs, :count).by(0)
        end
      end
    end

    context "with invalid parameters" do
      it "does not create a new Run" do
        expect do
          post "/users/#{user.id}/runs", headers: valid_headers,
               params: { run: invalid_attributes }, as: :json
        end.to change(Run, :count).by(0)
      end

      it "renders a JSON response with errors for the new run" do
        post "/users/#{user.id}/runs",
             params: { run: invalid_attributes }, headers: valid_headers, as: :json
        expect(response).to have_http_status(:unprocessable_entity)
      end
    end
  end

  describe "PATCH /update" do
    context 'when user is a regular user' do
      context "with valid parameters" do
        let(:run) { create :run, user: user }
        let(:new_attributes) do
          { date: 2.hours.ago }
        end

        it "updates the requested run" do
          patch "/users/#{user.id}/runs/#{run.id}",
                params: { run: new_attributes }, headers: valid_headers, as: :json
          run.reload
          expect(response).to be_successful
        end

        it "renders a JSON response with the run" do
          patch "/users/#{user.id}/runs/#{run.id}",
                params: { run: new_attributes }, headers: valid_headers, as: :json
          expect(response).to have_http_status(:ok)
        end
      end

      context "with invalid parameters" do
        context 'when params are invalid' do
          let!(:run) { create :run, user: user }

          it "renders a JSON response with errors for the run" do
            patch "/users/#{user.id}/runs/#{run.id}",
                  params: { run: invalid_attributes }, headers: valid_headers, as: :json
            expect(response).to have_http_status(:unprocessable_entity)
          end
        end
      end
    end

    context 'when user is admin' do
      context "with valid parameters" do
        let(:user) { create :user, :admin }
        let(:regular_user) { create :user, :regular_user }
        let(:run) { create :run, user: regular_user }

        context 'when updating the values of a run' do
          let(:new_attributes) do
            { date: 2.hours.ago }
          end

          it "updates the requested run" do
            patch "/users/#{regular_user.id}/runs/#{run.id}",
                  params: { run: new_attributes }, headers: valid_headers, as: :json
            run.reload
            expect(response).to be_successful
          end

          it "renders a JSON response with the run" do
            patch "/users/#{regular_user.id}/runs/#{run.id}",
                  params: { run: new_attributes }, headers: valid_headers, as: :json
            expect(response).to have_http_status(:ok)
            expect(run.reload.date).to eq new_attributes[:date].to_date
          end
        end
      end

      context "with invalid parameters" do
        let!(:run) { create :run, user: user }

        it "renders a JSON response with errors for the run" do
          patch "/users/#{user.id}/runs/#{run.id}",
                params: { run: invalid_attributes }, headers: valid_headers, as: :json
          expect(response).to have_http_status(:unprocessable_entity)
        end
      end
    end

    context 'when user is a user manager' do
      let(:user) { create :user, :user_manager }
      let(:regular_user) { create :user, :regular_user }
      let(:run) { create :run, user: regular_user }
      let(:new_attributes) do
        { date: 2.hours.ago }
      end

      it "should return forbidden status code" do
        patch "/users/#{regular_user.id}/runs/#{run.id}",
              params: { run: new_attributes }, headers: valid_headers, as: :json

        expect(response).to have_http_status(:forbidden)
      end
    end
  end

  describe "DELETE /destroy" do
    let!(:run) { create :run, user: user }

    context 'when the user is a regular user' do
      context 'when trying to destroy run belongs to his runs' do
        it "destroys the requested run" do
          expect do
            delete "/users/#{user.id}/runs/#{run.id}", headers: valid_headers, as: :json
          end.to change(Run, :count).by(-1)
        end
      end

      context 'when trying to destroy another user run' do
        let!(:another_run) { create :run }

        it 'should not destroy that run' do
          delete "/users/#{user.id}/runs/#{another_run.id}", headers: valid_headers, as: :json

          expect(response).to have_http_status(:not_found)
        end

        it "should not destroy the requested run" do
          expect do
            delete "/users/#{user.id}/runs/#{another_run.id}", headers: valid_headers, as: :json
          end.to change(Run, :count).by(0)
        end
      end
    end

    context 'when the user is an admin' do
      let(:user) { create :user, :admin }
      let(:run) { create :run }

      context 'when trying to destroy a run' do
        it "destroys the requested run" do
          expect do
            delete "/users/#{run.user.id}/runs/#{run.id}", headers: valid_headers, as: :json
          end.to change(Run, :count).by(-1)
        end
      end
    end

    context 'when the user is a user manager' do
      let(:user) { create :user, :user_manager }
      let(:run) { create :run }
      it 'should not destroy that run' do
        delete "/users/#{run.user.id}/runs/#{run.id}", headers: valid_headers, as: :json

        expect(response).to have_http_status(:forbidden)
      end
    end
  end
end
