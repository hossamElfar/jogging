FactoryBot.define do
  factory :run do
    user
    lat { 48.8567 }
    lng { 2.3508 }
    weather { nil }
    date { Time.zone.now }
    time_sec { Faker::Number.between(from: 1, to: 10) }
    distance_m { Faker::Number.between(from: 1, to: 10) }
    location { 'France' }
  end
end
