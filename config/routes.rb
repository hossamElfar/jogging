Rails.application.routes.draw do
  devise_for :users, path: 'auth/users', controllers: { sessions: 'users/sessions', registrations: 'users/registrations' }
  devise_scope :user do
    resources :users do
      resources :runs do
        collection do
          get 'report'
        end
      end
    end
  end
end
