# frozen_string_literal: true

rails_env = Rails.env || 'development'
sidekiq_config = Rails.application.config_for('sidekiq').with_indifferent_access

Sidekiq.configure_server do |config|
  config.redis = {
    url: "redis://#{sidekiq_config[:host]}:#{sidekiq_config[:port]}/#{sidekiq_config[:db]}",
    namespace: "sidekiq_#{rails_env}",
    network_timeout: sidekiq_config[:timeout]
  }
end

Sidekiq.configure_client do |config|
  config.redis = {
    url: "redis://#{sidekiq_config[:host]}:#{sidekiq_config[:port]}/#{sidekiq_config[:db]}",
    namespace: "sidekiq_#{rails_env}",
    network_timeout: sidekiq_config[:timeout]
  }
end

Sidekiq::Logging.logger.level = Logger::WARN if Rails.env.production?
