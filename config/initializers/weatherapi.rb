weatherapi_config = Rails.application.config_for('weatherapi').with_indifferent_access
WEATHERAPI_API_KEY = weatherapi_config[:api_key]
