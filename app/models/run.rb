class Run < ApplicationRecord
  # Relations
  belongs_to :user

  before_save :sync_speed
  # Callbacks
  after_create :update_weather

  # Validations
  validates :lat, numericality: { greater_than_or_equal_to: -90, less_than_or_equal_to: 90 }, allow_blank: true
  validates :lng, numericality: { greater_than_or_equal_to: -180, less_than_or_equal_to: 180 }, allow_blank: true
  validates :time_sec, numericality: { greater_than: 0 }, presence: true
  validates :distance_m, numericality: { greater_than: 0 }, presence: true
  validates :weather, numericality: true, allow_blank: true
  validates :date, date: true, presence: true
  validates :location, presence: { if: -> { lat.blank? && lng.blank? } }
  validates :lat, :lng, presence: { if: -> { location.blank? } }

  # Class methods
  def self.search_records(filters)
    query = where(user_id: filters[:conditions][:user_id])
    return query if filters[:conditions][:dynamic_filters].blank?

    query.where(filters[:conditions][:dynamic_filters])
  end

  def self.report(filters)
    query = where(user_id: filters[:conditions][:user_id])

    if filters[:conditions][:dynamic_filters].blank?
      avg_distance = JSON.parse(query.group_by_week(:date).average(:distance_m).chart_json)
      avg_speed = JSON.parse(query.group_by_week(:date).average(:speed).chart_json)
    else
      avg_distance = JSON.parse(
        query.
        where(filters[:conditions][:dynamic_filters]).
        group_by_week(:date).average(:distance_m).chart_json
      )
      avg_speed = JSON.parse(
        query.
        where(filters[:conditions][:dynamic_filters]).
        group_by_week(:date).average(:speed).chart_json
      )
    end

    merge_aggregations(avg_distance, avg_speed)
  end

  # Instance methods
  def update_weather
    RunWeatherSetter.perform_async(id)
  end

  def sync_speed
    self.speed = speed_from_distance_and_time
  end

  def speed_from_distance_and_time
    distance_m / time_sec
  end

  def self.merge_aggregations(avg_distance, avg_speed)
    res = []
    avg_distance.each_with_index do |distance_agg, idx|
      res << {
        week: distance_agg.first,
        average_distance: distance_agg.second.to_f.round(4),
        average_speed: avg_speed[idx].second.to_f.round(4)
      }
    end

    Kaminari.paginate_array res
  end
end
