class User < ApplicationRecord
  # Authentication
  devise :database_authenticatable, :registerable, :validatable, :trackable,
         :jwt_authenticatable, jwt_revocation_strategy: JwtDenylist

  # Enums
  enum role: { user: 0, user_manager: 1, admin: 2 }

  # Relations
  has_many :runs, dependent: :destroy

  # Class methods
  def self.search_records(filters)
    if filters.blank?
      all
    else
      where(filters[:conditions][:dynamic_filters])
    end
  end

  # Instance methods
  def jwt_payload
    {
      email: email,
      role: role
    }
  end
end
