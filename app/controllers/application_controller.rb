class ApplicationController < ActionController::API
  PER_PAGE = 10

  def collection_meta_info(collection)
    {
      current_page: collection.current_page,
      next_page: collection.next_page,
      per_page: collection.limit_value,
      prev_page: collection.prev_page,
      total_pages: collection.total_pages,
      total_count: collection.total_count
    }
  end

  rescue_from ActiveRecord::RecordNotFound do |exception|
    render json: {
      message: "#{exception.model.split(/(?=[A-Z])/).join(' ')} not found"
    }, status: :not_found
  end

  rescue_from ActionController::ParameterMissing do |exception|
    render json: { message: exception.message }, status: :unprocessable_entity
  end

  rescue_from ArgumentError do |exception|
    render json: { message: exception.message }, status: :unprocessable_entity
  end

  rescue_from Pundit::NotAuthorizedError do |exception|
    render json: { message: exception.message }, status: :forbidden
  end

  rescue_from ActiveRecord::StatementInvalid do |_exception|
    render json: { message: 'invalid query' }, status: :bad_request
  end
end
