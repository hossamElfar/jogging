# frozen_string_literal: true

module Users
  class SessionsController < Devise::SessionsController
    include Authenticatable

    before_action :authenticate!, only: [:destroy]

    respond_to :json

    private

    def respond_with(resource, _opts = {})
      if resource.id
        render json: resource.decorate.to_json, status: :created
      else
        render json: { message: 'unauthorized' }, status: :unauthorized
      end
    end

    def respond_to_on_destroy
      head :ok
    end
  end
end
