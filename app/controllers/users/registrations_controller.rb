# frozen_string_literal: true

module Users
  class RegistrationsController < Devise::RegistrationsController
    include Authenticatable

    before_action :authenticate!, only: [:show, :destroy, :update]

    respond_to :json

    # GET /resource/profile
    def show
      render json: current_user.to_json, status: :ok
    end

    # PUT /resource
    def update
      current_user.update!(user_params)

      render json: current_user.to_json, status: :ok
    end

    protected

    def configure_sign_up_params
      devise_parameter_sanitizer.permit(:sign_up, keys: [:name])
    end

    private

    def user_params
      params.require(:user).permit([:name])
    end

    rescue_from ActiveRecord::RecordNotFound do |exception|
      render json: {
        message: "#{exception.model.split(/(?=[A-Z])/).join(' ')} not found"
      }, status: :not_found
    end

    rescue_from ActionController::ParameterMissing do |exception|
      render json: { message: exception.message }, status: :bad_request
    end
  end
end
