class RunPolicy < ApplicationPolicy
  def index?
    user.admin? || user.user?
  end

  def show?
    if user.admin?
      true
    elsif user.user?
      record.user_id.eql? user.id
    else
      false
    end
  end

  def create?
    index?
  end

  def update?
    show?
  end

  def destroy?
    show?
  end

  def report?
    index?
  end
end
