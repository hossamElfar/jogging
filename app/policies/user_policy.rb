class UserPolicy < ApplicationPolicy
  def index?
    if user.user?
      user.id.eql? record.id
    else
      user.admin? || user.user_manager?
    end
  end

  def show?
    index?
  end

  def create?
    index?
  end

  def update?
    if user.admin?
      true
    elsif user.user_manager?
      !record.admin?
    else
      record.id.eql?(user.id) && !record.admin? && !record.user_manager?
    end
  end

  def destroy?
    update?
  end
end
