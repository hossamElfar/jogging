class RunDecorator < ApplicationDecorator
  delegate_all

  def to_json(*_args)
    {
      id: id,
      location_data: {
        latitude: lat,
        longitude: lng,
        location: location
      },
      weather: weather,
      distance: distance_m,
      time: time_sec,
      speed: {
        value: speed.round(3),
        unit: 'm/s'
      },
      date: date
    }
  end
end
